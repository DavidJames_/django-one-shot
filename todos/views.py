from django.shortcuts import render, get_object_or_404, redirect
from todos.models import TodoList, TodoItem
from todos.forms import TodoListForm, TodoNewItemForm

# Create your views here.


def todo_list_list(request):
    todo_lists = TodoList.objects.all()
    context = {
        "todo_list_list": todo_lists,
    }
    return render(request, 'Todolist/list.html', context)

def todo_list_detail(request, id):
    todo_item = get_object_or_404(TodoList, id=id)
    context = {
        "list_details": todo_item,
    }
    return render(request, "Todolist/details.html", context)

def todo_list_create(request):
    if request.method == "POST":
        form = TodoListForm(request.POST)
        if form.is_valid():
            list = form.save()
            return redirect("todo_list_detail", id=list.id)
    else:
        form = TodoListForm()

    context = {
        "form": form,
    }
    return render(request, "Todolist/create.html", context)

def todo_list_update(request, id):
    list = get_object_or_404(TodoList, id=id)
    if request.method == "POST":
        form = TodoListForm(request.POST, instance=list)
        if form.is_valid():
            form.save()
            return redirect("todo_list_list")
    else:
        form = TodoListForm(instance=list)

    context = {
        "list_object": list,
        "form": form,
    }
    return render(request, "Todolist/update.html", context)

def todo_list_delete(request, id):
    list_details = TodoList.objects.get(id=id)
    if request.method == "POST":
        list_details.delete()
        return redirect("todo_list_list")
    return render(request, "Todolist/delete.html")

def todo_item_create(request):
    if request.method == "POST":
        form = TodoNewItemForm(request.POST)
        if form.is_valid():
            form.save()
            return redirect("todo_list_list")
    else:
        form = TodoNewItemForm()

    context = {
        "form": form,
    }
    return render(request, "Todolist/createtask.html", context)

def todo_item_edit(request, id):
    items = get_object_or_404(TodoItem, id=id)
    if request.method == "POST":
        form = TodoNewItemForm(request.POST, instance=items)
        if form.is_valid():
            items = form.save()
            return redirect('todo_list_list')
    else:
        form = TodoNewItemForm(instance=items)

    context = {
        "task_item": TodoList.objects,
        "form": form,
    }
    return render(request, "Todolist/updatetask.html", context)
